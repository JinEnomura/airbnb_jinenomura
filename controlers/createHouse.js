const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/airbnbV7");

const locationSchema = new mongoose.Schema({
  location: String,
  homes: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "House"
    }
  ]
});

const housesSchema = new mongoose.Schema({
  name: String,
  beds: Number,
  price: Number,
  rating: Number,
  main_image: String
});

const House = mongoose.model("House", housesSchema);
const Location = mongoose.model("Location", locationSchema);

Location.create({ Location: "Aljezur" })
  .then(function(location) {
    console.log("sccess", location);
  })
  .catch(function(err) {
    console.log(err);
  });

House.create({
  name: "Casa do Alvaro",
  beds: 6,
  price: 199.99,
  rating: 4,
  main_image: "http://www.tecto.com.br/Images/Galeria/84/508.jpg"
})
  .then(function(house) {
    Location.find({ location: "Aljezur" }).then(function(location) {
      location[0].homes.push(house._id);
      location[0].save();
    });
  })
  .catch(function(err) {
    console.log(err);
  });
