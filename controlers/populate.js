const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/airbnbV7");

const locationSchema = new mongoose.Schema({
  location: String,
  homes: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "home"
    }
  ]
});

const housesSchema = new mongoose.Schema({
  name: String,
  beds: Number,
  price: Number,
  rating: Number,
  main_image: String
});

const Location = mongoose.model("Location", locationSchema);
const Home = mongoose.model("Home", housesSchema);

Location.create({location: "APTOS"})
.then(function(location){
    console.log("success", location)
})
.catch(function(err){
    console.log(err)
})

Home.create(/*{
    name: "Splendido Loft Appartamento Vicino a Piazza Dei Spagna",
    beds: 4,
    price: 169,
    main_image: "/images/c9db035f-7239-4b52-a8fa-b3e8622c8171.jpg",
    rating: 5
  },
  {
    name: "Modern Apartment with View of Piazza Del Popolo",
    beds: 4,
    price: 89,
    main_image: "/images/b8262e99-4f56-4c42-adc6-6f0a0d2a4d2d.jpg",
    rating: 2
  },
  {
    name: "Campo de' Fiori Deluxe Apartment",
    beds: 4,
    price: 129,
    main_image: "/images/ae327b04-5ed4-4017-84ba-4744ce5dc43e.jpg",
    rating: 3
  },
  {
    name: "Romantic Suite Apartment near Colosseum and the Forum",
    beds: 4,
    price: 65,
    main_image: "/images/7d7bd244-c01b-4f4a-af8b-4698db1b871c.jpg",
    rating: 5
}{
  name: "SUNSET CAVE HOUSE IN OIA",
  beds: 6,
  price: 215,
  main_image: "/images/881d76c6-4de6-4808-9f0b-36462b6c48a2.webp",
  rating: 5
}*/{
  name: "Mushroom Dome Cabin: #1 on airbnb in the world",
  beds: 2,
  price: 113,
  main_image: "/images/3ab8f121_original.jpg",
  rating: 5
})
  .then(function(home) {
    console.log("saved", home);
    // return house;
    Location.find({ location: "APTOS" }).then(function(location) {
        console.log(location[0]);
        console.log("casa:", home);
        location[0].homes.push(home._id);
        console.log(location[0].homes)
        location[0].save();
    });
  })
  .catch(function(err) {
    console.log(err);
  });

  // -------------------------- //

Location.find({location:"APTOS"}).populate("homes")
.then(function(location){
    console.log("saving populated homes", location[0].homes)
})
.catch(function(err){
    console.log(err)
})