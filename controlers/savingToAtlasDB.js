const mongoose = require("mongoose");


const processEnv = "mongodb+srv://Jin:8qORy4x2B3AuW0Om@cluster1-p3xok.mongodb.net/test?retryWrites=true"

mongoose.connect(

  processEnv, {
    useNewUrlParser: true
});

const housesSchema = new mongoose.Schema({
  houses: Array
});

const Houses = mongoose.model("Houses", housesSchema);

var romeAtlasHouses = Houses({
  houses: [
    {
      city: "rome",
      title: "Splendido Loft Appartamento Vicino a Piazza Dei Spagna",
      price: 169,
      imgURL: "/images/c9db035f-7239-4b52-a8fa-b3e8622c8171.jpg",
      stars: 5,
      description: "Verified Entire Apartment"
    },
    {
      city: "rome",
      title: "Modern Apartment with View of Piazza Del Popolo",
      price: 89,
      imgURL: "/images/b8262e99-4f56-4c42-adc6-6f0a0d2a4d2d.jpg",
      stars: 5,
      description: "Verified Entire Apartment"
    },
    {
      city: "rome",
      title: "Campo de' Fiori Deluxe Apartment",
      price: 129,
      imgURL: "/images/ae327b04-5ed4-4017-84ba-4744ce5dc43e.jpg",
      stars: 5,
      description: "Verified Entire Apartment"
    },
    {
      city: "rome",
      title: "Romantic Suite Apartment near Colosseum and the Forum",
      price: 65,
      imgURL: "/images/7d7bd244-c01b-4f4a-af8b-4698db1b871c.jpg",
      stars: 5,
      description: "Verified Entire Apartment"
    }
  ]
});

romeAtlasHouses.save();