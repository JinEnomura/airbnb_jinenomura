const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const homesSchema = new mongoose.Schema({
  name: String,
  beds: Number,
  price: Number,
  rating: Number,
  main_image: String
});

const Home = mongoose.model("Home", homesSchema);


module.exports = Home;