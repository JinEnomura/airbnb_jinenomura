const mongoose = require("mongoose");
const uri =
  "mongodb+srv://Jin:8qORy4x2B3AuW0Om@cluster1-p3xok.mongodb.net/test?retryWrites=true";

var connection = function() {
  mongoose
    .connect(
      //   uri,
      process.env.MONGODB_URI || "mongodb://localhost:27017/airbnbV7",
      // "mongodb://localhost:27017/airbnbV7",
      { useNewUrlParser: true }
    )
    .then(() => {
      console.log("Connected to mongo database");
    })
    .catch(err => {
      console.log("Error connecting mongo database", err);
    });
};

module.exports = connection;
