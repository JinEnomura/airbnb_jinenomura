const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const mongodb = require("mongodb");
const MongoClient = require("mongodb").MongoClient;
const uri =
  "mongodb+srv://Jin:8qORy4x2B3AuW0Om@cluster1-p3xok.mongodb.net/test?retryWrites=true";
const client = new MongoClient(uri, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
const urlencodedParser = bodyParser.urlencoded({ extended: true });
var locationExistsInDB = false;

const Location = require("./models/location");
const Home = require("./models/home");
const connection = require("./models/connection");

const app = express();

connection();

app.set("view engine", "ejs");
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.render("airbnb", function(err, html) {
    res.send(html);
  });
});

app.get("/s/:location/all", async (req, res) => {
  const searchPlace = req.params.location;
  console.log("searchPlace: ", searchPlace);

  const locationSearch = new Promise((resolve, reject) => {
    // console.log(searchPlace);
    Location.findOne({ location: searchPlace })
      .then(function(objectoRetornado) {
        console.log(objectoRetornado.location);
        if (objectoRetornado.location === req.params.location) {
          locationExistsInDB = true;
        } else {
          locationExistsInDB = false;
        }
        // console.log("location exists :", locationExistsInDB);
        resolve(locationExistsInDB);
      })
      .catch(function(err) {
        console.log(err);
        reject(err);
      });
  });

  // console.log("location search :", locationSearch);

  async function correrAssincrono() {
    return await locationSearch;
  }

  const doesLocationExists = await correrAssincrono();

  if (doesLocationExists === true) {
    Location.findOne({ location: searchPlace })
      .populate("homes")
      .exec(function(err, location) {
        if (err) {
          return handleError(err);
        }
        // console.log("location: ", location);
        // console.log("location homes name is: ", location.homes[0].name);
        else {
          res.render("airbnbLocationSearch.ejs", {
            city: location.location,
            houses: location.homes
          });
        }
      });
  } else {
    res.send("We found no results for that query");
  }
});

app.get("/s/restaurants", (req, res) => {
  res.send("You arrived to the restaurants search route");
});

app.get("/s/experiences", (req, res) => {
  res.send("You arrived to the experiences search route");
});

app.get("/s/homes", async (req, res) => {
  res.render("homes");
});

app.get("/rooms/:id", async (req, res, next) => {

  console.log("req.params.id is: ", req.params.id);

  const returnHomeById = new Promise(function(resolve, reject){
    resolve(Home.findById(req.params.id))
    reject(err);
  })

  await returnHomeById
  .then(val => {
    res.render("home.ejs", {houses: val}),
    next()
  })
  .catch(err => {
    next(err)
  })

  //----------------------

//   const returnHomeById = new Promise((resolve, reject) => {
//     Home.findById(req.params.id)
//       .then(function(objectoRetornado) {
//         console.log(objectoRetornado);
//         console.log(objectoRetornado._id);
//         if (objectoRetornado._id == req.params.id) {
//           homeExistsInDB = true;
//           homeReturnedFromId = objectoRetornado;
//         } else {
//           homeExistsInDB = false;
//         }
//         console.log("home exists :", homeReturnedFromId);
//         resolve(homeReturnedFromId);
//       })
//       .catch(function(err) {
//         console.log(err);
//         reject(err);
//       });
//   });

//   async function teste() {
//     try {
//         let str = await returnHomeById;
//         console.log(str);
//         res.render("home", {houses: str});
//         next();
//     } catch (e) {
//         console.log(e);
//         next(e);
//     }

// }

// teste();

//--------------------------

// try {
//   // Do something
//   next();
// } catch (error) {
//   next(error);
// }
  
});

app.get("/:location/homes/new", (req, res) => {
  res.render("new_home", {
    qs: req.query
  });
});

app.post("/:location/homes", urlencodedParser, (req, res) => {
  var searchPlace = req.params.location;
  // console.log(searchPlace);

  Home.create({
    name: req.body.name,
    beds: req.body.beds,
    price: req.body.price,
    rating: req.body.rating,
    main_image: req.body.main_image
  })
    .then(function(home) {
      location
        .findOne({ locations: searchPlace })
        .populate("houses")
        .then(function(location) {
          // console.log("casa:", house);
          location.homes.push(home._id);
          location.save();
          // console.log(location);
          location.redirect("/s/" + searchPlace + "/all");
        });
    })
    .catch(function(err) {
      console.log(err);
    });
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on Port ${port}...`));
